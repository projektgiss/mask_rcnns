# -*- coding: utf-8 -*-
# File: coco.py

import json
import os

import numpy as np
import pandas as pd
import tifffile as tiff
import torch
import torchvision
from maskrcnn_benchmark.structures.bounding_box import BoxList
from maskrcnn_benchmark.structures.segmentation_mask import SegmentationMask
from PIL import Image

ir_images_to_remove = ['136ir_cutted_tiff.tiff',
                       '40ir_cutted_tiff.tiff',
                       '43ir_cutted_tiff.tiff',
                       '50ir_cutted_tiff.tiff']


class GISS_IRDataset(object):

    def __init__(self,
                 ann_file,
                 root,
                 transforms=None):

        self.ann_file = ann_file
        self.root = root
        self.transforms = transforms
        self.class_names = [
            'cars', 'small_trucks', 'truck',
            'building', 'person', 'container', 'misc']

        self.df_annots = pd.read_csv(self.ann_file)
        self.df_annots['filename_cut'] = self.df_annots.filename.apply(
            lambda x: x.split('\\')[-1])
        self.df_annots = self.df_annots.assign(
            full_filename=self.df_annots.apply(
                lambda x: '{}/{}'.format(self.root, x['filename']), axis=1))
        self.df_annots = self.df_annots[
            ~self.df_annots.filename_cut.isin(ir_images_to_remove)]
        print('Dropped: {}'.format(ir_images_to_remove))
        self.ids = self.df_annots.full_filename.unique().tolist()

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, idx):

        df_filename = self.df_annots[
            self.df_annots['full_filename'] == self.ids[idx]]

        boxes = df_filename.apply(
            lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        boxes = np.stack(
            boxes.values, axis=0)
        filename_full = self.ids[idx]

        img = tiff.imread(filename_full)
        img = (img / np.max(img)) * 255
        img = np.repeat(np.expand_dims(img, axis=-1), 4, axis=-1)
        # img = np.repeat(np.expand_dims(img, axis=-1), 3, axis=-1)
        img = Image.fromarray(img.astype(np.uint8))

        boxes = torch.as_tensor(boxes).reshape(-1, 4)
        target = BoxList(boxes, img.size, mode="xyxy")

        classes = df_filename['class'].values
        classes = torch.tensor(classes)
        target.add_field("labels", classes)

        target = target.clip_to_image(remove_empty=True)

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target, idx

    def get_img_info(self, index):

        filename_full = self.ids[index]
        img = tiff.imread(filename_full)
        img = (img / np.max(img)) * 255
        img = np.repeat(np.expand_dims(img, axis=-1), 4, axis=-1)
        img = Image.fromarray(img.astype(np.uint8))

        return {"height": img.size[1], "width": img.size[0]}
