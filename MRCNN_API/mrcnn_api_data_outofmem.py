import gc
import glob
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import utils
from config import Config
from matplotlib import patches
from skimage.io import imread
from skimage.transform import resize
from tqdm import tqdm


class Mrcnn_Data_API(object):

    def __init__(self,
                 image_shape=(1024, 1024),
                 resize_and_pad=True,
                 verbose=False,
                 depth_basedir=None,
                 ):

        self.image_shape = image_shape
        self.resize_and_pad = resize_and_pad
        self.verbose = verbose
        self.depth_basedir = depth_basedir

    def prepare_mrcnn_dataset_dir(self, src_train, src_valid=None,
                                  mode='train',
                                  dataset_config=None):
        """
        Dataset preparation function for out-of-memory training.
        src_train - training set directory (can be used as test set).
        src_valid - validation set directory.
        """

        if self.verbose:
            print('\nPreparing MRCNN datasets.\n')

        if mode == 'train':
            if self.verbose:
                print('Preparing training and validation sets.')

            dataset_train = dataset_config()
            dataset_train.load_data(src_train)
            dataset_train.prepare()

            dataset_val = dataset_config()
            dataset_val.load_data(src_valid)
            dataset_val.prepare()

            if self.verbose:
                print('Number of training images : {}'.format(
                    len(dataset_train.image_info)))
                print('Number of validation images : {}\n'.format(
                    len(dataset_val.image_info)))

            return dataset_train, dataset_val

        if mode == 'test':
            if self.verbose:
                print('Preparing test set.')

            dataset_test = dataset_config()
            dataset_test.load_data(src_train)
            dataset_test.prepare()

            if self.verbose:
                print('Number of test images : {}\n'.format(
                    len(dataset_test.image_info)))

            return dataset_test

    def process_data(self,
                     src_images,
                     labels,
                     image_filetype='JPG'):
        """
        Parse selected folder, read images, resize them to size set in
        self.image_shape and create masks based on bounding box coordinates
        in labels DataFrame.
        """

        if self.verbose:
            print('\nProcessing data in: {}'.format(src_images))
            print('Selecting {} images.'.format(image_filetype))
            print('Resized image shape: {}\n'.format(self.image_shape))

        dir_list = ['images', 'masks', 'shapes', 'classes', 'names']
        for i in dir_list:
            os.makedirs('{}{}'.format(src_images, i), exist_ok=True)

        images_list = labels.filename.unique()

        for img in tqdm(images_list):

            if self.verbose:
                print('\nProcessing image:', img)

            y_image = labels[labels.filename == img]

            try:
                image, mask, mask_class = self.__process_image_mask(y_image)

                image_savename = y_image.filename.unique()[0].split('.')[
                    0].replace('/', '_')

                pd.to_pickle(image, src_images +
                             'images/{}.pkl'.format(image_savename))
                pd.to_pickle(mask, src_images +
                             'masks/{}.pkl'.format(image_savename))
                pd.to_pickle(image.shape, src_images +
                             'shapes/{}.pkl'.format(image_savename))
                pd.to_pickle(mask_class, src_images +
                             'classes/{}.pkl'.format(image_savename))
                pd.to_pickle(y_image.filename.unique()[0], src_images +
                             'names/{}.pkl'.format(image_savename))
            except FileNotFoundError:
                img_src = y_image.full_filename.unique()[0]
                print('Could not load: {}'.format(img_src))

        return

    def process_data_parallel(self,
                              src_images,
                              labels,
                              image_filetype='JPG'):
        """
        Parse selected folder, read images, resize them to size set in
        self.image_shape and create masks based on bounding box coordinates
        in labels DataFrame.
        """

        if self.verbose:
            print('\nProcessing data in: {}'.format(src_images))
            print('Selecting {} images.'.format(image_filetype))
            print('Resized image shape: {}\n'.format(self.image_shape))

        dir_list = ['images', 'masks', 'shapes', 'classes', 'names']
        for i in dir_list:
            os.makedirs('{}{}'.format(src_images, i), exist_ok=True)

        images_list = labels.filename.unique()
        y_images = []

        for img in tqdm(images_list):

            if self.verbose:
                print('\nProcessing image:', img)

            y_image = labels[labels.filename == img]
            y_images.append(y_image)

            try:
                image, mask, mask_class = self.__process_image_mask(y_image)

                image_savename = y_image.filename.unique()[0].split('.')[
                    0].replace('/', '_')

                pd.to_pickle(image, src_images +
                             'images/{}.pkl'.format(image_savename))
                pd.to_pickle(mask, src_images +
                             'masks/{}.pkl'.format(image_savename))
                pd.to_pickle(image.shape, src_images +
                             'shapes/{}.pkl'.format(image_savename))
                pd.to_pickle(mask_class, src_images +
                             'classes/{}.pkl'.format(image_savename))
                pd.to_pickle(y_image.filename.unique()[0], src_images +
                             'names/{}.pkl'.format(image_savename))
            except FileNotFoundError:
                img_src = y_image.full_filename.unique()[0]
                print('Could not load: {}'.format(img_src))

            return

    def perform_image_check(self,
                            src_images,
                            image_names,
                            y,
                            index=None):
        """
        Visualize image and masks to check if they are properly processed.
        """

        if index is None:
            index = np.random.randint(0, len(image_names))
        if not isinstance(image_names, pd.Series):
            image_names = pd.Series(image_names)

        image_check = '{}{}'.format(src_images, image_names[index])
        image_filename = image_check.split('/')[-1]
        img = imread(image_check)

        print('\nChecking image {} from: {}'.format(image_filename, src_images))
        plt.imshow(img)
        self.__show_bbs(y, img, image_filename)

        return

    def __get_depth_channel(self, y_image):

        y_depth = y_image.copy()
        y_depth['depth_set'] = y_depth.set.apply(
            lambda x: '{}_depth'.format(x))
        y_depth['depth_filename'] = y_depth.filename.apply(
            lambda x: x.split('.')[0] + '_depth.png')
        y_depth['full_depth_filename'] = y_depth.apply(lambda x: '{}{}/{}'.format(
            self.depth_basedir, x.depth_set, x.depth_filename.split('/')[-1]), axis=1)

        assert len(y_image.filename.unique()) == 1
        img_src = y_image.full_filename.unique()[0]
        depth_img_src = y_depth.full_depth_filename.unique()[0]
        if self.verbose:
            print('Loading 4-th depth channel: {}'.format(depth_img_src))

        image = imread(img_src)
        if len(image) == 2:
            if self.verbose:
                print('Image constituting of two parts, choosing image only.')
            image = image[0]

        im_depth = imread(depth_img_src)
        if len(im_depth) == 2:
            if self.verbose:
                print('Depth image constituting of two parts, choosing image only.')
            im_depth = im_depth[0]
        if im_depth.shape[:2] != image.shape[:2]:
            im_depth = resize(im_depth, image.shape[:2])
        print(im_depth.shape, image.shape)
        im_depth = np.expand_dims(im_depth, axis=-1)
        image = np.concatenate([image, im_depth], axis=-1)
        return image

    def __process_image_mask(self, y_image):

        if self.depth_basedir is not None:
            image = self.__get_depth_channel(y_image)
        else:
            assert len(y_image.filename.unique()) == 1
            img_src = y_image.full_filename.unique()[0]
            image = imread(img_src)
            if len(image) == 2:
                if self.verbose:
                    print('Image constituting of two parts, choosing image only.')
                image = image[0]

        image_shape = image.shape
        mask = np.zeros(
            (image.shape[0], image.shape[1], y_image.shape[0]), dtype=np.uint8)
        mask_class = []

        if self.verbose:
            print('Original image shape: {}\nMask shape: {}'.format(
                image.shape, mask.shape))

        for i in range(y_image.shape[0]):
            coords = np.array([y_image.iloc[i]['ymin'],
                               y_image.iloc[i]['ymax'],
                               y_image.iloc[i]['xmin'],
                               y_image.iloc[i]['xmax']], dtype=np.int)
            mask[coords[0]:coords[1], coords[2]:coords[3], i] = 1.
            mask_class.append(y_image.iloc[i]['class'])

        if self.resize_and_pad:
            image, window, scale, padding = utils.resize_image(
                image,
                min_dim=self.image_shape[0],
                max_dim=self.image_shape[1],
                padding=True)
            mask = utils.resize_mask(mask, scale, padding)

        if self.verbose:
            print('Image min: {}, max: {}'.format(
                image.min(), image.max()))
            print('Mask min: {}, max: {}'.format(
                mask.min(), mask.max()))
            print('Final image shape: {}'.format(image.shape))
            print('Final mask shape: {}'.format(mask.shape))
        assert np.min(mask) == 0. and np.max(
            mask) == 1., "Mask values must be either 0 or 1."

        return image, mask, mask_class

    def __get_center(self,
                     x):
        x['x'] = x.xmax - x.xmin
        x['y'] = x.ymax - x.ymin
        return x

    def __show_bbs(self,
                   y,
                   image,
                   image_name):

        y = y.apply(lambda x: self.__get_center(x), axis=1)
        y_image = y[y.filename == image_name]

        fig, ax = plt.subplots()
        ax.imshow(image)

        for i in range(y_image.shape[0]):
            ax.add_patch(patches.Rectangle((y_image.iloc[i, :]['xmin'],
                                            y_image.iloc[i, :]['ymin']),
                                           y_image.iloc[i, :]['x'],
                                           y_image.iloc[i, :]['y'], facecolor='red'))

        return
