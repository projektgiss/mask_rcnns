logdir='train_log/resnet101_coco_fpn_w1_1333'
load='pretrained_models/COCO-ResNet101-MaskRCNN_nohead.npz'

evaluation_set='val'  # val or test
gpu=0


python train.py \
  --gpu $gpu \
  --logdir $logdir \
  --load $load \
  --evaluation_set $evaluation_set
