from matplotlib import patches


def get_center(x):
    x['x'] = x.xmax - x.xmin
    x['y'] = x.ymax - x.ymin
    return x


def show_bbs(y,
             image,
             image_name):

    y = y.apply(lambda x: get_center(x), axis=1)
    y_image = y[y.image_id == image_name]
    assert y_image.shape[0] > 0, 'No detections to show!'

    fig, ax = plt.subplots()
    ax.imshow(image)

    for i in range(y_image.shape[0]):
        plt_colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        color = np.random.choice(plt_colors, 1)[0]
        ax.add_patch(patches.Rectangle((y_image.iloc[i, :]['xmin'],
                                        y_image.iloc[i, :]['ymin']),
                                       y_image.iloc[i, :]['x'],
                                       y_image.iloc[i,
                                                    :]['y'], facecolor='none',
                                       edgecolor=color, lw=2))

    return


def load_eval_scores(preds_files):

    df_scores = []

    for i in preds_files:
        score_info = pd.read_json(i, typ='series')
        df_scores.append(score_info)

    df_scores = pd.DataFrame(df_scores)
    max_mAP = df_scores['mAP_valid'].max()

    print('Maximum mAP: {:.5f}'.format(max_mAP))

    return df_scores
