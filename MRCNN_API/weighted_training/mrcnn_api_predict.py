import glob
import os
from argparse import ArgumentParser

from mrcnn_api import GISSConfig, Mrcnn_API

parser = ArgumentParser()
parser.add_argument("--weights_file",
                    help="Provide trained model weights file.",
                    type=str, default='')
parser.add_argument("--image_filename",
                    help="Provide image filename.",
                    type=str, default='')
args = parser.parse_args()
print('\nRunning with parameters: {}\n'.format(args))


config = GISSConfig()
class_names = ['BG', 'car', 'building',
               'human', 'truck', 'container', 'winter_car']

mrcnn_model = Mrcnn_API(config, model_path=args.weights_file)

img_pad = mrcnn_model.load_single_image(
    args.image_filename, resize_and_pad=True)
img_preds = mrcnn_model.predict_image(
    img_pad, class_names, visualize_predictions=False)
df_preds = mrcnn_model.create_predictions_df(args.image_filename, img_preds)
print('Resulting predictions dataframe:\n{}\n'.format(df_preds))
