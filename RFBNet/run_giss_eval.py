#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import print_function

import argparse
import logging
import os
import pdb
import pickle
import sys
import time

import cv2
import numpy as np
import pandas as pd
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.utils.data as data
import torchvision.transforms as transforms
from data import (COCO_300, COCO_512, VOC_300, VOC_512, AnnotationTransform,
                  BaseTransform, COCO_mobile_300, COCODetection, COCOroot,
                  GISSDetection, VOCDetection, VOCroot)
from layers.functions import Detect, PriorBox
from map_eval import *
from torch.autograd import Variable
from utils.nms_wrapper import nms
from utils.timer import Timer

version = 'RFB_vgg'
size = '512'
dataset = 'GISS'
trained_model = 'weights/RFB_vgg_VOC_epoches_495.pth'
save_folder = 'eval/'
cuda = True
cpu = False
retest = False
num_classes = 8
debug = False


def get_center(x):
    x['x'] = x.xmax - x.xmin
    x['y'] = x.ymax - x.ymin
    return x


def show_bbs(image, y_subset, i=0):

    plt.rcParams['figure.figsize'] = (16, 12)

    color = 'r'
    fig, ax = plt.subplots()
    ax.imshow(image)
    for i in range(len(y_subset)):
        y_image = y_subset.iloc[i, :]
        ax.add_patch(patches.Rectangle((y_image['xmin'],
                                        y_image['ymin']),
                                       y_image['x'],
                                       y_image['y'], facecolor='none',
                                       edgecolor=color, lw=2))

    return


def calculate_iou(true_box, pred_boxes):
    """
        Single (!) true_box and pred_boxes must be numpy array object
    """
    intersection = np.maximum(0, np.minimum(true_box[:, 2], pred_boxes[:, 2]) - np.maximum(true_box[:, 0], pred_boxes[:, 0])) * np.maximum(
        0, np.minimum(true_box[:, 3], pred_boxes[:, 3]) - np.maximum(true_box[:, 1], pred_boxes[:, 1]))
    union = (true_box[:, 2] - true_box[:, 0]) * (true_box[:, 3] - true_box[:, 1]) + \
        (pred_boxes[:, 2] - pred_boxes[:, 0]) * \
        (pred_boxes[:, 3] - pred_boxes[:, 1]) - intersection
    result = intersection / union
    return result


def make_cross_iou_table(true_boxes, pred_boxes):
    iou_table = []
    for true_box in true_boxes:
        result = calculate_iou(np.expand_dims(true_box, axis=0), pred_boxes)
        iou_table.append(result)
    return np.array(iou_table)


def f1_scoring(TP, FP, FN):
    '''
    Gives the accuracy metrics for evaluating predictions.

    Arguments:
        TP (float): Number of true positives examples.
        FP (float): Number of false positives examples.
        FN (float): Number of false negatives examples.

    Returns:
        F1, precision, recall
    '''
    if TP == 0 and FN == 0:
        recall = 0
    else:
        recall = TP / (TP + FN)
    if TP == 0 and FP == 0:
        precision = 0
    else:
        precision = TP / (TP + FP)
    if precision * recall != 0:
        F1 = 2 * precision * recall / (precision + recall)
    else:
        F1 = 0

    return F1, precision, recall


def evaluate(ground_truth_csv, predict_box_csv, classes, iou_thresh=0.4, mode='INFO', scale_to=None):
    """
        scale_to -- need to be set while all coords in csv with predictions are normalized to one resolution
    """

    if isinstance(ground_truth_csv, pd.DataFrame) and isinstance(predict_box_csv, pd.DataFrame):
        df_gt = ground_truth_csv
        df_pb = predict_box_csv
    else:
        try:
            df_gt = pd.read_csv(ground_truth_csv, delimiter=',')
            df_pb = pd.read_csv(predict_box_csv, delimiter=',')
        except:
            print('Cannot read CSV with ground truths and predictions.')

    TP_det_all, TP_class_all = 0, 0
    FN_det_all, FN_class_all = 0, 0
    FP_det_all, FP_class_all = 0, 0

    pb_filenames = df_pb['filename'].unique()

    no_of_classes = len(classes)
    classes_dict = {k: v for k, v in zip(classes, range(0, len(classes)))}
    print(classes_dict)

    confusion_matrix = np.zeros([no_of_classes - 1, no_of_classes - 1])

    for filename in pb_filenames:
        TP_det, TP_class = 0, 0
        FN_det, FN_class = 0, 0
        FP_det, FP_class = 0, 0
        # try:
        true_labels = df_gt[df_gt['filename'] == filename]
        if scale_to is not None:
            scale = scale_to / np.max(true_labels[['width', 'height']].max())
            true_boxes = true_labels[[
                'xmin', 'ymin', 'xmax', 'ymax']].values * scale
        else:
            true_boxes = true_labels[['xmin', 'ymin', 'xmax', 'ymax']].values
        pred_labels = df_pb[(df_pb['filename'] == filename)]
        pred_boxes = pred_labels[['xmin', 'ymin', 'xmax', 'ymax']].values
        iou_table = make_cross_iou_table(true_boxes, pred_boxes)
        which_matches = np.argmax(iou_table, axis=1)
        for i in range(len(true_boxes)):
            if iou_table[i, which_matches[i]] > iou_thresh:
                TP_det += 1
                true = true_labels['class'].iloc[i]
                pred = pred_labels['class'].iloc[which_matches[i]]
                # TODO
                # BAD DESIGN!!! trzeba zrobic slownik klasy->int
                confusion_matrix[classes_dict[true] -
                                 1, classes_dict[pred] - 1] += 1
                if classes_dict[true] == classes_dict[pred]:
                    TP_class += 1

        FN_det = len(true_boxes) - TP_det
        FN_class = len(true_boxes) - TP_class
        FP_det = len(pred_boxes) - TP_det
        FP_class = len(pred_boxes) - TP_class

        TP_det_all += TP_det
        TP_class_all += TP_class
        FN_det_all += FN_det
        FN_class_all += FN_class
        FP_det_all += FP_det
        FP_class_all += FP_class
        # except:
        #    print('Error: there is no \'{}\' file in csv with ground truth boxes.'.format(filename) )

    F1_det, precision_det, recall_det = f1_scoring(
        TP_det_all, FP_det_all, FN_det_all)
    F1_class, precision_class, recall_class = f1_scoring(
        TP_class_all, FP_class_all, FN_class_all)
    cm_df = pd.DataFrame(confusion_matrix, columns=range(1, no_of_classes))
    cm_df.index = cm_df.index + 1

    conf_stats = {
        'TP_det': TP_det_all,
        'TP_all': TP_class_all,
        'FN_det': FN_det_all,
        'FN_all': FN_class_all,
        'FP_det': FP_det_all,
        'FP_all': FP_class_all
    }

    f_stats = {
        'F1_class': F1_class,
        'Precision_class': precision_class,
        'Recall_class': recall_class,
    }

    return cm_df, conf_stats, f_stats


def process_df_true(df_true_src, data_src):

    df_true = pd.read_csv(df_true_src)

    df_true.filename = df_true.filename.str.replace("\\", "/")
#     df_true['full_filename'] = df_true.filename.apply(
#         lambda x: '{}{}'.format(data_src, x))

    return df_true


if not os.path.exists(save_folder):
    os.mkdir(save_folder)


BASEDIR = '/home/w/projects/pw-giss/data/data/giss_images/original/'
LABELS_DIR = '/home/w/projects/pw-giss/data/models/labels/csv/labels_21_05_deg0/'
TRAIN_LABELS = LABELS_DIR + 'train_labels_21_05_deg0.csv'
VAL_LABELS = LABELS_DIR + 'val_labels_21_05_deg0.csv'
TEST_LABELS = LABELS_DIR + 'test_labels_21_05_deg0.csv'

device = 'cuda:0'
VOCroot = '../data/data/VOCdevkit'


if dataset == 'VOC':
    train_sets = [('2007', 'trainval'), ('2012', 'trainval')]
    cfg = (VOC_300, VOC_512)[size == '512']
if dataset == 'GISS':
    train_sets = [('2018', 'train')]
    cfg = (VOC_300, VOC_512)[size == '512']
else:
    train_sets = [('2014', 'train'), ('2014', 'valminusminival')]
    cfg = (COCO_300, COCO_512)[size == '512']

if version == 'RFB_vgg':
    from models.RFB_Net_vgg import build_net
elif version == 'RFB_E_vgg':
    from models.RFB_Net_E_vgg import build_net
elif version == 'RFB_mobile':
    from models.RFB_Net_mobile import build_net
    cfg = COCO_mobile_300
else:
    print('Unkown version!')

priorbox = PriorBox(cfg)
with torch.no_grad():
    priors = priorbox.forward()
    if cuda:
        priors = priors.cuda()


def test_net(save_folder, net, detector, cuda, testset, transform,
             max_per_image=300, thresh=0.005, debug=False):

    if not os.path.exists(save_folder):
        os.mkdir(save_folder)
    # dump predictions and assoc. ground truth to text file for now

    num_images = len(testset)
    if debug:
        num_images = 20

    all_boxes = [[[] for _ in range(num_images)]
                 for _ in range(num_classes)]

    _t = {'im_detect': Timer(), 'misc': Timer()}
    det_file = os.path.join(save_folder, 'detections.pkl')

    if retest:
        f = open(det_file, 'rb')
        all_boxes = pickle.load(f)
        print('Evaluating detections')
        testset.evaluate_detections(all_boxes, save_folder)
        return

    for i in range(num_images):
        img = testset.pull_image(i)
        scale = torch.Tensor([img.shape[1], img.shape[0],
                              img.shape[1], img.shape[0]])
        with torch.no_grad():
            x = transform(img).unsqueeze(0)
            if cuda:
                x = x.cuda()
                scale = scale.cuda()

        _t['im_detect'].tic()
        out = net(x)      # forward pass
        boxes, scores = detector.forward(out, priors)
        detect_time = _t['im_detect'].toc()
        boxes = boxes[0]
        scores = scores[0]

        boxes *= scale
        boxes = boxes.cpu().numpy()
        scores = scores.cpu().numpy()
        # scale each detection back up to the image

        _t['misc'].tic()

        for j in range(1, num_classes):
            inds = np.where(scores[:, j] > thresh)[0]
            if len(inds) == 0:
                all_boxes[j][i] = np.empty([0, 5], dtype=np.float32)
                continue
            c_bboxes = boxes[inds]
            c_scores = scores[inds, j]
            c_dets = np.hstack((c_bboxes, c_scores[:, np.newaxis])).astype(
                np.float32, copy=False)

            keep = nms(c_dets, 0.45, force_cpu=cpu)
            c_dets = c_dets[keep, :]
            all_boxes[j][i] = c_dets
        if max_per_image > 0:
            image_scores = np.hstack([all_boxes[j][i][:, -1]
                                      for j in range(1, num_classes)])
            if len(image_scores) > max_per_image:
                image_thresh = np.sort(image_scores)[-max_per_image]
                for j in range(1, num_classes):
                    keep = np.where(all_boxes[j][i][:, -1] >= image_thresh)[0]
                    all_boxes[j][i] = all_boxes[j][i][keep, :]

        nms_time = _t['misc'].toc()

        if i % 20 == 0:
            print('im_detect: {:d}/{:d} {:.3f}s {:.3f}s'
                  .format(i + 1, num_images, detect_time, nms_time))
            _t['im_detect'].clear()
            _t['misc'].clear()

    return all_boxes


if __name__ == '__main__':
    # load net
    img_dim = (300, 512)[size == '512']
    net = build_net('test', img_dim, num_classes)    # initialize detector
    state_dict = torch.load(trained_model)
    # create new OrderedDict that does not contain `module.`

    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        head = k[:7]
        if head == 'module.':
            name = k[7:]  # remove `module.`
        else:
            name = k
        new_state_dict[name] = v
    net.load_state_dict(new_state_dict)
    net.eval()
    print('Finished loading model!')
    print(net)
    # load data
    if dataset == 'VOC':
        testset = VOCDetection(
            VOCroot, [('2007', 'test')], None, AnnotationTransform())
    elif dataset == 'COCO':
        testset = COCODetection(
            COCOroot, [('2014', 'minival')], None)
    elif dataset == 'GISS':
        testset = GISSDetection(BASEDIR, VAL_LABELS, None)
    else:
        print('Only VOC and COCO dataset are supported now!')
    if cuda:
        net = net.cuda()
        cudnn.benchmark = True
    else:
        net = net.cpu()

    # evaluation
    # top_k = (300, 200)[dataset == 'COCO']

    top_k = 100
    detector = Detect(num_classes, 0, cfg)
    save_folder = os.path.join(save_folder, dataset)
    rgb_means = ((104, 117, 123), (103.94, 116.78, 123.68))[
        version == 'RFB_mobile']

    st = time.time()

    all_boxes = test_net(save_folder, net, detector, cuda, testset,
                         BaseTransform(net.size, rgb_means, (2, 0, 1)),
                         top_k, thresh=0.01, debug=debug)

    print('\nTime it took for eval: {:.3f}s'.format(time.time() - st))

    class_names = [0, 1, 2, 3, 4, 5, 6, 7]
    class_names_raw = ['BG', 'cars', 'small_trucks',
                       'truck', 'building', 'person', 'container', 'misc']

    class_dict = {'cars': 1,
                  'small_trucks': 2,
                  'truck': 3,
                  'building': 4,
                  'person': 5,
                  'container': 6,
                  'misc': 7}

    eval_name = trained_model.split('/')[1].split('.')[0]

    # Eval part:
    image_names = testset.pull_ids()

    df_pred = []
    for c in range(len(all_boxes)):
        boxes_c = all_boxes[c]
        for i in range(len(boxes_c)):
            img_boxes = pd.DataFrame(boxes_c[i])
            img_boxes['class'] = class_names_raw[c]
            img_boxes['filename'] = image_names[i]
            df_pred.append(img_boxes)
    df_pred = pd.concat(df_pred, ignore_index=True, sort=False)
    df_pred.columns = ['class', 'filename',
                       'xmin', 'ymin', 'xmax', 'ymax', 'score']
    df_pred['class'] = df_pred['class'].map(class_dict)

    df_preds_ = df_pred.copy()
    df_preds_[['xmin', 'xmax', 'ymin', 'ymax']] = df_preds_[
        ['xmin', 'xmax', 'ymin', 'ymax']].astype(np.int)

    df_true = process_df_true(VAL_LABELS, BASEDIR)

    df_true = df_true.rename(columns={'score': 'confidence'})
    df_preds_ = df_preds_.rename(columns={'score': 'confidence'})

    map_train, train_no_preds = output_mAP(df_true, df_preds_)
    cf, cf_stats, f_stats = evaluate(df_true, df_preds_, class_names)

    cf.columns = class_names_raw[1:]
    cf.index = class_names_raw[1:]
    cf.to_csv('eval/{}_cf_val.csv'.format(eval_name))

    f_stats = pd.DataFrame.from_dict(f_stats, orient='index').T
    f_stats['mAP'] = map_train
    f_stats.to_csv('eval/{}_f_stats_val.csv'.format(eval_name))

    print('mAP: {:.3f}'.format(map_train))
    print('\n{}\n{}'.format(cf, f_stats))
