import glob
import os
import pprint
import sys

import numpy as np
import pandas as pd
from skimage.color import rgb2gray
from skimage.filters import frangi, gaussian, sobel
from skimage.io import concatenate_images, imread, imread_collection, imshow
from skimage.morphology import label
from skimage.restoration import (denoise_bilateral, denoise_nl_means,
                                 denoise_tv_bregman, denoise_tv_chambolle,
                                 denoise_wavelet, estimate_sigma)
from skimage.transform import resize
from tqdm import tqdm

# Data-related


def load_data(src, mode='basic'):
    if mode == 'basic':
        X = pd.read_pickle('../data/X_train_256x256.pkl')
        y = pd.read_pickle('../data/y_train_256x256.pkl')
        X_test = pd.read_pickle('../data/X_test_256x256.pkl')
    train_shapes = pd.read_pickle('../data/train_shapes.pkl')
    test_shapes = pd.read_pickle('../data/test_shapes.pkl')
    return X, y, X_test, train_shapes, test_shapes


def get_data(src, resize=False, img_shape=None, masks=False,):

    ids = next(os.walk(src))[1]

    X = []
    if masks:
        y = []
    img_shapes = []

    sys.stdout.flush()

    for n, id_ in tqdm(enumerate(ids), total=len(ids)):
        path = src + id_
        img = imread(path + '/images/' + id_ + '.png')[:, :, :3]
        img_shapes.append(img.shape)
        if resize:
            img = resize(img, img_shape, mode='constant', preserve_range=True)
        X.append(img)

        if masks:
            mask = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.bool)
            for mask_file in next(os.walk(path + '/masks/'))[2]:
                mask_ = imread(path + '/masks/' + mask_file)
                if resize:
                    mask_ = resize(mask_, img_shape, mode='constant',
                                   preserve_range=True)
                mask_ = np.expand_dims(mask_, axis=-1)
                mask = np.maximum(mask, mask_)
            y.append(mask)

    if masks:
        return X, y, img_shapes, ids
    else:
        return X, img_shapes, ids

# Submissions


def rle_encoding(x):
    dots = np.where(x.T.flatten() == 1)[0]
    run_lengths = []
    prev = -2
    for b in dots:
        if (b > prev + 1):
            run_lengths.extend((b + 1, 0))
        run_lengths[-1] += 1
        prev = b
    return run_lengths


def prob_to_rles(x, cutoff=0.5):
    lab_img = label(x > cutoff)
    for i in range(1, lab_img.max() + 1):
        yield rle_encoding(lab_img == i)


def create_submission(test_df, predictions, sizes_test, submission_name):

    preds_test_upsampled = []
    for i in range(len(predictions)):
        preds_test_upsampled.append(resize(np.squeeze(predictions[i]),
                                           (sizes_test[i][0],
                                            sizes_test[i][1]),
                                           mode='constant', preserve_range=True))

    new_test_ids = []
    rles = []
    for n, id_ in enumerate(test_ids):
        rle = list(prob_to_rles(preds_test_upsampled[n]))
        rles.extend(rle)
        new_test_ids.extend([id_] * len(rle))

    sub = pd.DataFrame()
    sub['ImageId'] = new_test_ids
    sub['EncodedPixels'] = pd.Series(rles).apply(
        lambda x: ' '.join(str(y) for y in x))
    sub.to_csv(
        '../submissions/{}.csv'.format(submission_name), index=False)
    print('Submission head:', sub.head(10))
    return sub

# Array-related


def do_clip(arr, max_value):
    arr_clip = np.clip(arr, (1 - max_value) / 7, max_value)
    arr_clip[arr_clip >= max_value] = 1
    return arr_clip


def normalize_std(X, X_test):
    assert (len(X.shape) == 4)
    assert (len(X_test.shape) == 4)

    X_normalized = np.empty(X.shape)
    X_test_norm = np.empty(X_test.shape)

    X_std = np.std(X)
    X_mean = np.mean(X)

    X_normalized = (X - X_mean)
    X_normalized = (X - X_mean) / X_std
    X_test_norm = (X_test - X_mean)
    X_test_norm = (X_test - X_mean) / X_std

    for i in range(X.shape[0]):
        X_normalized[i] = ((X_normalized[i] - np.min(X_normalized[i])) /
                           (np.max(X_normalized[i]) - np.min(X_normalized[i])))
        X_normalized[i][X_normalized[i] > 1.] = 1.
        X_normalized[i][X_normalized[i] < 0.] = 0.

    for i in range(X_test.shape[0]):
        X_test_norm[i] = ((X_test_norm[i] - np.min(X_test_norm[i])) /
                          (np.max(X_test_norm[i]) - np.min(X_test_norm[i])))
        X_test_norm[i][X_test_norm[i] > 1.] = 1.
        X_test_norm[i][X_test_norm[i] < 0.] = 0.

    print('Arrays properly normalized to 0-1 range.')
    print(np.min(X_normalized), np.max(X_normalized))
    print(np.min(X_test_norm), np.max(X_test_norm))
    return X_normalized, X_test_norm


def normalize_along_channel(X, X_test):
    assert (len(X.shape) == 4)
    assert (len(X_test.shape) == 4)

    X_normalized = np.empty(X.shape)
    X_test_norm = np.empty(X_test.shape)

    stds = []
    means = []

    for i in range(X_test.shape[-1]):
        stds.append(np.std(X[:, :, :, i]))
        means.append(np.mean(X[:, :, :, i]))
    print(stds, '\n', means)

    for c in range(X_test.shape[-1]):

        X_normalized[:, :, :, c] = (X[:, :, :, c] - means[c]) / stds[c]
        X_test_norm[:, :, :, c] = (X_test[:, :, :, c] - means[c]) / stds[c]

        for i in range(X.shape[0]):
            X_normalized[i, :, :, c] = ((X_normalized[i, :, :, c] - np.min(X_normalized[i, :, :, c])) /
                                        (np.max(X_normalized[i, :, :, c]) - np.min(X_normalized[i, :, :, c])))
            X_normalized[i, :, :, c][X_normalized[i, :, :, c] > 1.] = 1.
            X_normalized[i, :, :, c][X_normalized[i, :, :, c] < 0.] = 0.

        for i in range(X_test.shape[0]):
            X_test_norm[i, :, :, c] = ((X_test_norm[i, :, :, c] - np.min(X_test_norm[i, :, :, c])) /
                                       (np.max(X_test_norm[i, :, :, c]) - np.min(X_test_norm[i, :, :, c])))
            X_test_norm[i, :, :, c][X_test_norm[i, :, :, c] > 1.] = 1.
            X_test_norm[i, :, :, c][X_test_norm[i, :, :, c] < 0.] = 0.

    print('Each channel normalized to 0-1 range.')
    print(np.min(X_normalized), np.max(X_normalized))
    print(np.min(X_test_norm), np.max(X_test_norm))
    return X_normalized, X_test_norm


# Other


def load_predictions(src, load_oof=True, contains=None, not_contains=None):
    if load_oof:
        train_files = sorted(glob.glob('{}train/*.pkl'.format(src)))
    else:
        train_files = sorted(glob.glob('{}valid/*.pkl'.format(src)))
    test_files = sorted(glob.glob('{}test/*.pkl'.format(src)))
    if contains is not None:
        train_files = [x for x in train_files if contains in x]
        test_files = [x for x in test_files if contains in x]
    if not_contains is not None:
        train_files = [x for x in train_files if not_contains not in x]
        test_files = [x for x in test_files if not_contains not in x]
    print('\nTrain files:\n')
    pprint.pprint(train_files)
    print('\nTest files:\n')
    pprint.pprint(test_files)
    valid_preds = []
    test_preds = []
    for i in train_files:
        valid_preds.append(pd.read_pickle(i))
    for i in test_files:
        test_preds.append(pd.read_pickle(i))
    return valid_preds, test_preds
