# from .voc import VOCDetection, AnnotationTransform, detection_collate, VOC_CLASSES
from .coco import COCODetection
from .config import *
from .data_augment import *
from .giss import *
from .voc0712 import (VOC_CLASSES, AnnotationTransform, VOCDetection,
                      VOCSegmentation, detection_collate)
