import colorsys
import gc
import glob
import math
import os
import random
import re
import sys
import time

import avro.schema as avro_schema
import matplotlib
import matplotlib.pyplot as plt
import model as modellib
import numpy as np
import pandas as pd
import utils
import visualize
from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter
from config import Config
from confluent_kafka import Consumer, KafkaError, Producer, avro
from confluent_kafka.avro import AvroConsumer, AvroProducer
from confluent_kafka.avro.serializer import SerializerError
from matplotlib import patches
from model import log
from skimage.io import concatenate_images, imread, imread_collection, imshow
from skimage.transform import resize
from sklearn.utils import shuffle
from tqdm import tqdm


class MRCNN_API(object):

    def __init__(self,
                 config,
                 img_shape=(1024, 1024),
                 model_path=None,
                 model_dir=None):

        self.config = config
        self.img_shape = img_shape
        self.model_path = model_path
        self.model_dir = model_dir if model_dir is not None else os.path.join(
            os.getcwd(), "logs")

    def resize_pad_images(self, image_, mask_=None):

        image, window, scale, padding = utils.resize_image(
            image_,
            min_dim=self.config.IMAGE_MIN_DIM,
            max_dim=self.config.IMAGE_MAX_DIM,
            padding=self.config.IMAGE_PADDING)
        if mask_ is not None:
            mask = utils.resize_mask(mask_, scale, padding)
            return image, mask

        return image

    def load_single_image(self, src, resize_and_pad=True, verbose=False):

        if verbose:
            print('Loading image: {}'.format(src))

        image = imread(src)
        if len(image) == 2:
            image = image[0]
        if verbose:
            print('Original image shape: {}'.format(image.shape))

        if resize_and_pad:
            image = self.resize_pad_images(image)
            if verbose:
                print('Image padded to: {}'.format(image.shape))

        return image

    def predict_image(self, image, class_names, visualize_predictions=False):

        inference_config = InferenceConfig()

        model = modellib.MaskRCNN(mode="inference",
                                  config=inference_config,
                                  model_dir=self.model_dir)

        assert self.model_path != "", "Provide path to trained weights"
        print("Loading weights from: {}".format(self.model_path))
        model.load_weights(self.model_path, by_name=True)

        t_start = time.time()
        results = model.detect([image], verbose=1)
        t_end = time.time()
        print('Time it took for inference: {}'.format(t_end - t_start))

        r = results[0]

        if visualize_predictions:
            visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'],
                                        class_names, r['scores'], ax=get_ax(
                                            size=18),
                                        figsize=(14, 14))
        return r

    def create_predictions_df(self, filename, r, verbose=False):

        if verbose:
            print('Image filename: {}'.format(filename))

        df_bb = pd.DataFrame(r['rois'])
        df_bb.columns = ['ymin', 'xmin', 'ymax', 'xmax']
        df_bb['filename'] = filename
        df_bb['class'] = r['class_ids']
        df_bb['confidence'] = r['scores']
        df_bb = df_bb.apply(lambda x: get_center(x), axis=1)

        return df_bb


class GISSConfig(Config):
    """Configuration for training on the GISS dataset with 3 RGB channels.
    Derives from the base Config class and overrides values specific
    to the toy shapes dataset.
    """
    # Give the configuration a recognizable name
    NAME = "giss_depth0"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 6

    IMAGE_MIN_DIM = 1024
    IMAGE_MAX_DIM = 1024
    IMAGE_PADDING = True

    # The strides of each layer of the FPN Pyramid. These values
    # are based on a Resnet101 backbone.
    BACKBONE_STRIDES = [4, 8, 16, 32, 64]

    # Use smaller anchors because our image and objects are small
    RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)  # anchor side in pixels

    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    TRAIN_ROIS_PER_IMAGE = 128

    # If enabled, resizes instance masks to a smaller size to reduce
    # memory load. Recommended when using high-resolution images.
    USE_MINI_MASK = True
    MINI_MASK_SHAPE = (56, 56)  # (height, width) of the mini-mask

    # Non-max suppression threshold to filter RPN proposals.
    # You can reduce this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.7

    # Max number of final detections
    DETECTION_MAX_INSTANCES = 128

    # Minimum probability value to accept a detected instance
    # ROIs below this threshold are skipped
    DETECTION_MIN_CONFIDENCE = 0.7

    # Non-maximum suppression threshold for detection
    DETECTION_NMS_THRESHOLD = 0.3

    # Learning rate and momentum
    # The Mask RCNN paper uses lr=0.02, but on TensorFlow it causes
    # weights to explode. Likely due to differences in optimzer
    # implementation.
    LEARNING_RATE = 0.001
    LEARNING_MOMENTUM = 0.9

    # Weight decay regularization
    WEIGHT_DECAY = 0.0001

    # Use RPN ROIs or externally generated ROIs for training
    # Keep this True for most situations. Set to False if you want to train
    # the head branches on ROI generated by code rather than the ROIs from
    # the RPN. For example, to debug the classifier head without having to
    # train the RPN.
    USE_RPN_ROIS = True

    MEAN_PIXEL = np.array([123.7, 116.8, 103.9])

    STEPS_PER_EPOCH = 26
    VALIDATION_STEPS = 7


class InferenceConfig(GISSConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


def get_center(x):
    x['x'] = x.xmax - x.xmin
    x['y'] = x.ymax - x.ymin
    return x


def get_ax(rows=1, cols=1, size=8):
    _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
    return ax


# Kafka


def kafka_produce_image_filenames(src):

    def acked(err, msg):
        if err is not None:
            print("Failed to deliver message: {0}: {1}"
                  .format(msg.value(), err.str()))
        else:
            print("Message produced: {0}".format(msg.value()))

    p = Producer({'bootstrap.servers': 'localhost:9092'})

    try:
        for val in glob.glob(src + '*.JPG'):
            p.produce('mytopic', '{}'.format(val), callback=acked)
            p.poll(0.5)

    except KeyboardInterrupt:
        pass

    p.flush(30)

    return


def kafka_consume_image_filenames():

    settings = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'mygroup',
        'client.id': 'client-1',
        'enable.auto.commit': True,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'}
    }

    c = Consumer(settings)
    c.subscribe(['mytopic'])
    messages = []

    try:
        while True:
            msg = c.poll(0.1)
            if msg is None:
                continue
            elif not msg.error():
                msg_str = msg.value().decode('utf-8')
                print('Received message: {0}'.format(msg_str))
                messages.append(msg_str)
            elif msg.error().code() == KafkaError._PARTITION_EOF:
                print('End of partition reached {0}/{1}'
                      .format(msg.topic(), msg.partition()))
            else:
                print('Error occured: {0}'.format(msg.error().str()))

    except KeyboardInterrupt:
        pass

    finally:
        c.close()

    return messages


def kafka_consume_and_predict(mrcnn_model, class_names):

    settings = {
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'mygroup',
        'client.id': 'client-1',
        'enable.auto.commit': True,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'}
    }

    c = Consumer(settings)
    c.subscribe(['mytopic'])
    messages = []

    try:
        while True:
            msg = c.poll(0.1)
            if msg is None:
                continue
            elif not msg.error():
                msg_str = msg.value().decode('utf-8')
                print('Received message: {0}'.format(msg_str))
                img_pad = mrcnn_model.load_single_image(
                    msg_str, resize_and_pad=True)
                img_preds = mrcnn_model.predict_image(
                    img_pad, class_names, visualize_predictions=False)
                df_preds = mrcnn_model.create_predictions_df(
                    msg_str, img_preds)
                print('Resulting predictions dataframe:\n{}\n'.format(df_preds))
                print('Saving to AVRO.')
                save_to_avro(df_preds, schema_filename='mrcnn_b.avsc')
                print('Predictions DF successfully saved.\n')
            elif msg.error().code() == KafkaError._PARTITION_EOF:
                print('End of partition reached {0}/{1}'
                      .format(msg.topic(), msg.partition()))
            else:
                print('Error occured: {0}'.format(msg.error().str()))

    except KeyboardInterrupt:
        pass

    finally:
        c.close()

    return messages


def save_to_avro(df_preds, schema_filename='mrcnn.avsc', data_filename='mrcnn.avro'):

    schema = avro_schema.Parse(open(schema_filename, 'rb').read())
    writer = DataFileWriter(open('mrcnn.avro', 'wb'), DatumWriter(), schema)

    for i in range(df_preds.shape[0]):
        dfs = df_preds.iloc[i, :].astype(str)
        writer.append({'filename': dfs['filename'],
                       'class': dfs['class'],
                       'confidence': dfs['confidence'],
                       'xmin': dfs['xmin'],
                       'ymin': dfs['ymin'],
                       'xmax': dfs['xmax'],
                       'ymax': dfs['ymax'],
                       'x': dfs['x'],
                       'y': dfs['y']})
    writer.close()

    return


def read_avro(data_filename='mrcnn.avro'):

    predicitions_list = []

    reader = DataFileReader(open(data_filename, 'rb'), DatumReader())
    for user in reader:
        predicitions_list.append(user)
    reader.close()

    predicitions_list = pd.DataFrame(predicitions_list)

    return predicitions_list
