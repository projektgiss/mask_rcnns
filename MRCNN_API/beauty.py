X, masks = api_data.prepare_data_for_training(
    src_data=giss_data_config['dst'],
    src_depth=giss_data_config['src_depth'],
    set_filename=giss_data_config['set_filename'],
    add_depth_map=giss_data_config['add_depth_map'])
