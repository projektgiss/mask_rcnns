# -*- coding: utf-8 -*-
# File: coco.py

import json
import os

import cv2
import numpy as np
import pandas as pd
from tqdm import tqdm


class mAPevaluate(object):

    def __init__(self,
                 df_true,
                 df_preds,
                 iou_threshold=0.5):

        self.df_true = df_true
        self.df_preds = df_preds
        self.iou_threshold = iou_threshold
        self.class_dict = None

    def prepare_mAP_input(self, img_true, img_preds, img_id):

        gt_boxes = img_true.apply(
            lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        gt_boxes = np.stack(gt_boxes.values, axis=0).astype(np.float32)

        gt_class_ids = img_true['class'].values

        pred_boxes = img_preds.apply(
            lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        pred_boxes = np.stack(pred_boxes.values, axis=0).astype(np.float32)

        pred_class_ids = img_preds['class'].values
        pred_scores = img_preds['confidence'].values

        return gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores

    def compute_batch_ap(self):

        unique_filenames = self.df_true.filename.unique()

        imgs_no_preds = 0
        orig_missed_images = {}

        APs = []

        for i in unique_filenames:
            img_true = self.df_true[self.df_true.filename == i]
            img_preds = self.df_preds[self.df_preds.filename == i]
            if img_preds.shape[0] == 0 and img_true.shape[0] != 0:
                AP = 0
                precision = 0
                recall = 0
                imgs_no_preds += 1
                orig_missed_images[i] = img_true.shape[0]
            else:
                gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores = self.prepare_mAP_input(
                    img_true, img_preds, i)
                AP, precision, recall, overlaps = compute_ap(
                    gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores,
                    iou_threshold=self.iou_threshold)
            APs.append(AP)

        mAP = np.mean(APs)
        print('mAP at IoU threshold {}: {:.5f}'.format(self.iou_threshold, mAP))
        print('Images, where no objects were found: {}'.format(imgs_no_preds))

        return mAP, imgs_no_preds


def prepare_df_true(dataset, config):

    bboxes = []
    classes = []
    image_names = []

    for i in tqdm(range(len(dataset.image_info))):
        image, image_meta, class_ids, bbox, mask = modellib.load_image_gt(
            dataset, config, dataset.image_ids[i])
        for j in range(class_ids.shape[0]):
            bboxes.append(bbox[j])
            classes.append(class_ids[j])

            img_name = dataset.image_info[i]['path']
            img_part = img_name.replace('.pkl', '.JPG').split('/')[-1]
            img_part0 = img_part.split('DJI')[0][:-1]
            img_part1 = '{}{}'.format('DJI', img_part.split('DJI')[1])
            img_name_final = '/'.join([img_part0, img_part1])
            image_names.append(img_name_final)

    df_true = pd.DataFrame([bboxes, classes, image_names]).T
    df_true.columns = ['bbox', 'class', 'filename']
    df_true['ymin'] = df_true['bbox'].apply(lambda x: x[0])
    df_true['xmin'] = df_true['bbox'].apply(lambda x: x[1])
    df_true['ymax'] = df_true['bbox'].apply(lambda x: x[2])
    df_true['xmax'] = df_true['bbox'].apply(lambda x: x[3])

    return df_true


def output_mAP(df_true, df_preds):
    mAP, imgs_no_preds = mAPevaluate(df_true, df_preds).compute_batch_ap()
    return mAP, imgs_no_preds

# Based on Matterports MRCNN functions


def compute_iou(box, boxes, box_area, boxes_area):
    """Calculates IoU of the given box with the array of the given boxes.
    box: 1D vector [y1, x1, y2, x2]
    boxes: [boxes_count, (y1, x1, y2, x2)]
    box_area: float. the area of 'box'
    boxes_area: array of length boxes_count.

    Note: the areas are passed in rather than calculated here for
          efficency. Calculate once in the caller to avoid duplicate work.
    """
    # Calculate intersection areas
    y1 = np.maximum(box[0], boxes[:, 0])
    y2 = np.minimum(box[2], boxes[:, 2])
    x1 = np.maximum(box[1], boxes[:, 1])
    x2 = np.minimum(box[3], boxes[:, 3])
    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)
    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union
    return iou


def compute_overlaps(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].

    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes
    area1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1])
    area2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1])

    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))
    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]
        overlaps[:, i] = compute_iou(box2, boxes1, area2[i], area1)
    return overlaps


def trim_zeros(x):
    """It's common to have tensors larger than the available data and
    pad with zeros. This function removes rows that are all zeros.

    x: [rows, columns].
    """
    assert len(x.shape) == 2
    return x[~np.all(x == 0, axis=1)]


def compute_ap(gt_boxes, gt_class_ids,
               pred_boxes, pred_class_ids, pred_scores,
               iou_threshold=0.5):
    """Compute Average Precision at a set IoU threshold (default 0.5).

    Returns:
    mAP: Mean Average Precision
    precisions: List of precisions at different class score thresholds.
    recalls: List of recall values at different class score thresholds.
    overlaps: [pred_boxes, gt_boxes] IoU overlaps.
    """
    # Trim zero padding and sort predictions by score from high to low
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    pred_boxes = trim_zeros(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]

    # Compute IoU overlaps [pred_boxes, gt_boxes]
    overlaps = compute_overlaps(pred_boxes, gt_boxes)

    # Loop through ground truth boxes and find matching predictions
    match_count = 0
    pred_match = np.zeros([pred_boxes.shape[0]])
    gt_match = np.zeros([gt_boxes.shape[0]])
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] == 1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            if iou < iou_threshold:
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                match_count += 1
                gt_match[j] = 1
                pred_match[i] = 1
                break

    # Compute precision and recall at each prediction box step
    precisions = np.cumsum(pred_match) / (np.arange(len(pred_match)) + 1)
    recalls = np.cumsum(pred_match).astype(np.float32) / len(gt_match)

    # Pad with start and end values to simplify the math
    precisions = np.concatenate([[0], precisions, [0]])
    recalls = np.concatenate([[0], recalls, [1]])

    # Ensure precision values decrease but don't increase. This way, the
    # precision value at each recall threshold is the maximum it can be
    # for all following recall thresholds, as specified by the VOC paper.
    for i in range(len(precisions) - 2, -1, -1):
        precisions[i] = np.maximum(precisions[i], precisions[i + 1])

    # Compute mean AP over recall range
    indices = np.where(recalls[:-1] != recalls[1:])[0] + 1
    mAP = np.sum((recalls[indices] - recalls[indices - 1]) *
                 precisions[indices])

    return mAP, precisions, recalls, overlaps
