data_src = '/home/w/Projects/PW-GISS/data/data/giss_images/original/'
depth_src = '/home/w/Projects/PW-GISS/data/data/detector-depth/'
labels_src = '/home/w/Projects/PW-GISS/data/models/labels/csv/labels_21_05_deg0/'


labels_files = sorted(glob.glob(labels_src + '*.csv'))

train_csv = pd.read_csv(labels_files[1])
valid_csv = pd.read_csv(labels_files[2])
test_csv = pd.read_csv(labels_files[0])

train_csv.filename = train_csv.filename.str.replace("\\", "/")
valid_csv.filename = valid_csv.filename.str.replace("\\", "/")
test_csv.filename = test_csv.filename.str.replace("\\", "/")

train_csv['set'] = train_csv.filename.apply(lambda x: x.split('/')[0])
valid_csv['set'] = valid_csv.filename.apply(lambda x: x.split('/')[0])
test_csv['set'] = test_csv.filename.apply(lambda x: x.split('/')[0])

train_csv['full_filename'] = train_csv.filename.apply(lambda x: '{}{}'.format(data_src, x))
valid_csv['full_filename'] = valid_csv.filename.apply(lambda x: '{}{}'.format(data_src, x))
test_csv['full_filename'] = test_csv.filename.apply(lambda x: '{}{}'.format(data_src, x))

df_annots = pd.concat([train_csv, valid_csv, test_csv], axis=0)
df_annots.reset_index(inplace=True, drop=True)


df_annots['image_filename'] = df_annots.filename.apply(lambda x: x.split('/')[1])
df_annots['depth_set'] = df_annots.set.apply(lambda x: '{}_depth'.format(x))
df_annots['depth_filename'] = df_annots.image_filename.apply(lambda x: x.split('.')[0] + '_depth.png')
df_annots['full_depth_filename'] = df_annots.apply(lambda x: '{}{}/{}'.format(src_depth, x.depth_set, x.depth_filename), axis=1)
