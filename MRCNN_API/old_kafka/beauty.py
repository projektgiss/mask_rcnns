src_path = '/home/w/Projects/PW-GISS/data/DetectorSet2/'
src_depth = '/home/w/Projects/PW-GISS/input/90oblot_depth/'

load_test = False
use_depth = False


if load_test:
    X_test, y_test, test_filenames, test_shapes, test_classes = get_data(
        src_path, mode='test')

    X_d, y_d, test_filenames, test_shapes, test_classes = subset_depth_data(
        src_depth, X, y, test_filenames, test_shapes,
        test_classes, add_depth_map=use_depth)
    X_test_, y_test_ = resize_pad(X_d, y_d)

    dataset_test = GISSDataset()
    dataset_test.load_data(
        X_test_, y_test_, test_filenames, test_shapes, test_classes)
    dataset_test.prepare()

    print('Number of test images : {}'.format(len(dataset_test.image_info)))

    del X_test, y_test
    del X_test_, y_test_
    gc.collect()

else:
    X, y, train_filenames, train_shapes, train_classes = get_data(src_path)
    X_d, y_d, train_filenames, train_shapes, train_classes = subset_depth_data(
        src_depth, X, y,
        train_filenames, train_shapes,
        train_classes, add_depth_map=use_depth)
    X_, y_ = resize_pad(X_d, y_d)

    subset_border = int(len(X_) * 0.8)

    X_tr = X_[:subset_border]
    y_tr = y_[:subset_border]
    tr_ids = train_filenames[:subset_border]
    tr_shapes = train_shapes[:subset_border]
    tr_classes = train_classes[:subset_border]

    X_val = X_[subset_border:]
    y_val = y_[subset_border:]
    val_ids = train_filenames[subset_border:]
    val_shapes = train_shapes[subset_border:]
    val_classes = train_classes[subset_border:]

    dataset_train = GISSDataset()
    dataset_train.load_data(X_tr, y_tr, tr_ids, tr_shapes, tr_classes)
    dataset_train.prepare()

    dataset_val = GISSDataset()
    dataset_val.load_data(X_val, y_val, val_ids, val_shapes, val_classes)
    dataset_val.prepare()

    print('Number of training images : {}'.format(
        len(dataset_train.image_info)))
    print('Number of validation images : {}'.format(len(dataset_val.image_info)))

    del X_, y_, X, y
    del X_tr, y_tr, X_val, y_val
    gc.collect()
