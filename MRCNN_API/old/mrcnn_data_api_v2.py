import gc
import glob
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import utils
from config import Config
from matplotlib import patches
from skimage.io import (concatenate_images, imread, imread_collection, imsave,
                        imshow)
from skimage.transform import resize
from tqdm import tqdm


class Mrcnn_Data_API(object):

    def __init__(self,
                 image_shape=(1024, 1024),
                 resize_and_pad=True,
                 verbose=False,
                 ):

        self.image_shape = image_shape
        self.resize_and_pad = resize_and_pad
        self.verbose = verbose

    def prepare_mrcnn_dataset_dir(self, src1, src2=None,
                                  mode='train',
                                  dataset_config=None):
        """
        Dataset preparation function for out-of-memory training.
        src1 - training set directory.
        src2 - validation set directory.
        """

        if self.verbose:
            print('\nPreparing MRCNN datasets.\n')

        if mode == 'train':
            if self.verbose:
                print('Preparing training and validation sets.')

            dataset_train = dataset_config()
            dataset_train.load_data(src1)
            dataset_train.prepare()

            dataset_val = dataset_config()
            dataset_val.load_data(src2)
            dataset_val.prepare()

            if self.verbose:
                print('Number of training images : {}'.format(
                    len(dataset_train.image_info)))
                print('Number of validation images : {}\n'.format(
                    len(dataset_val.image_info)))

            return dataset_train, dataset_val

        if mode == 'test':
            if self.verbose:
                print('Preparing test set.')

            dataset_test = dataset_config()
            dataset_test.load_data(src1)
            dataset_test.prepare()

            if self.verbose:
                print('Number of test images : {}\n'.format(
                    len(dataset_test.image_info)))

            return dataset_test

    def prepare_mrcnn_dataset(self, src_data,
                              X, masks, filenames, shapes, classes,
                              mode='train',
                              valid_split_size=0.2,
                              dataset_config=None):
        """
        Dataset preparation function for in-memory training.
        """

        if self.verbose:
            print('\nPreparing MRCNN datasets.\nValidation split fraction: {}'.format(
                valid_split_size))

        if mode == 'train':
            subset_border = int(len(X) * (1 - valid_split_size))
            if self.verbose:
                print('Preparing training and validation sets.')
                print('\nSplitting on index: {}'.format(subset_border))

            X_tr = X[:subset_border]
            y_tr = masks[:subset_border]
            tr_ids = filenames[:subset_border]
            tr_shapes = shapes[:subset_border]
            tr_classes = classes[:subset_border]

            X_val = X[subset_border:]
            y_val = masks[subset_border:]
            val_ids = filenames[subset_border:]
            val_shapes = shapes[subset_border:]
            val_classes = classes[subset_border:]

            dataset_train = dataset_config()
            dataset_train.load_data(X_tr, y_tr, tr_ids, tr_shapes, tr_classes,
                                    src_data)
            dataset_train.prepare()

            dataset_val = dataset_config()
            dataset_val.load_data(X_val, y_val, val_ids,
                                  val_shapes, val_classes, src_data)
            dataset_val.prepare()

            if self.verbose:
                print('Number of training images : {}'.format(
                    len(dataset_train.image_info)))
                print('Number of validation images : {}\n'.format(
                    len(dataset_val.image_info)))

            del X_tr, y_tr, X_val, y_val
            del X, masks
            gc.collect()

            return dataset_train, dataset_val

        if mode == 'test':
            if self.verbose:
                print('Preparing test set.')

            dataset_test = dataset_config()
            dataset_test.load_data(
                X, masks, filenames, shapes, classes, src_data)
            dataset_test.prepare()

            if self.verbose:
                print('Number of test images : {}\n'.format(
                    len(dataset_test.image_info)))

            del X, masks
            gc.collect()

            return dataset_test

        return dataset_train, dataset_val

    def prepare_data_for_training(self,
                                  src_data,
                                  src_depth=None,
                                  set_filename='',
                                  mode='train',
                                  add_depth_map=False,
                                  ):
        """
        Load and process data for MRCNN training.
        Depth maps can be added as 4-th channel.
        """

        if self.verbose:
            print('\nLoading pickled sets of images.')

        X, masks, filenames, shapes, classes = self.load_data(
            src_data, set_filename=set_filename, mode=mode)

        if add_depth_map and src_depth is not None:
            if self.verbose:
                print('Add depth maps as 4th channel.')
            X, masks, filenames, shapes, classes = self.subset_depth_data(
                src_depth, X, masks,
                filenames, shapes,
                classes, add_depth_map=add_depth_map)

        X, masks = self.resize_pad_images(X, masks)
        if self.verbose:
            print('\nImages resized and padded.')

        return X, masks, filenames, shapes, classes

    def save_images_masks(self, X,
                          masks,
                          image_names,
                          image_shapes,
                          image_classes,
                          dst):
        """
        Save processed data as .pkl for quick access.
        """

        dst_images = dst + 'images/'
        dst_masks = dst + 'masks/'

        os.makedirs(dst_images, exist_ok=True)
        os.makedirs(dst_masks, exist_ok=True)

        for i in range(len(image_names)):
            pd.to_pickle(X[i], '{}{}_processed.pkl'.format(
                dst_images, image_names[i].split('.')[0]))
            pd.to_pickle(masks[i], '{}{}_processed.pkl'.format(
                dst_masks, image_names[i].split('.')[0]))

        pd.to_pickle(
            image_names, '{}/image_names_processed.pkl'.format(dst_images))
        pd.to_pickle(
            image_shapes, '{}/image_shapes_processed.pkl'.format(dst_images))
        pd.to_pickle(
            image_classes, '{}/image_classes_processed.pkl'.format(dst_images))

        return

    def load_data(self,
                  src,
                  set_filename='',
                  mode='train'):
        """
        Load processed data from .pkl files saved with save_images_masks function.
        """

        if self.verbose:
            print('\nLoading processed data files from: {}\n'.format(src))

        X = pd.read_pickle(src + '{}_{}_images.pkl'.format(set_filename, mode))
        masks = pd.read_pickle(
            src + '{}_{}_masks.pkl'.format(set_filename, mode))
        filenames = pd.read_pickle(
            src + '{}_{}_names.pkl'.format(set_filename, mode))
        shapes = pd.read_pickle(
            src + '{}_{}_shapes.pkl'.format(set_filename, mode))
        classes = pd.read_pickle(
            src + '{}_{}_classes.pkl'.format(set_filename, mode))

        return X, masks, filenames, shapes, classes

    def process_raw_images(self,
                           src_images,
                           y,
                           image_filetype='*',
                           save=False,
                           dst=None,
                           set_filename='',
                           mode='train'):
        """
        Process (resize and create masks) raw images .
        """

        if self.verbose:
            print('\nProcessing raw images into masks.')

        X, masks, image_names, image_shapes, image_classes = self.create_masks(
            src_images, y, image_filetype)

        if save and dst is not None:
            if self.verbose:
                print('\nSaving pickle files into: {}\n'.format(dst))
            pd.to_pickle(
                X, dst + '{}_{}_images.pkl'.format(set_filename, mode))
            pd.to_pickle(
                masks, dst + '{}_{}_masks.pkl'.format(set_filename, mode))
            pd.to_pickle(image_names, dst +
                         '{}_{}_names.pkl'.format(set_filename, mode))
            pd.to_pickle(image_shapes, dst +
                         '{}_{}_shapes.pkl'.format(set_filename, mode))
            pd.to_pickle(image_classes, dst +
                         '{}_{}_classes.pkl'.format(set_filename, mode))

        return

    def subset_depth_data(self,
                          src_depth_images,
                          X,
                          masks,
                          filenames,
                          shapes,
                          classes,
                          add_depth_map=False,
                          image_filetype='JPG',
                          depth_image_filetype='png'):
        """
        Pick intersection of RGB images and depth (or IR) maps,
        add mentioned maps as 4-th channel.
        """

        filenames_depth = pd.Series(
            np.sort(glob.glob(src_depth_images + '*.{}'.format(depth_image_filetype))))
        assert len(filenames_depth) > 0, 'No depth images found. Aborting.'

        filenames = pd.Series(filenames)
        if self.verbose:
            print('\nNumber of images with depth maps: {}'.format(
                len(filenames_depth)))

        imgnames_depth = filenames_depth.apply(lambda x: x.split(
            '/')[-1].replace('_depth', '').replace('{}'.format(depth_image_filetype),
                                                   '{}'.format(image_filetype)))
        imgnames_intersect = np.intersect1d(filenames, imgnames_depth)
        imgnames_intersect_index = filenames[filenames.isin(
            imgnames_depth)].index.astype(int).tolist()
        if self.verbose:
            print('Number of images with corresponding depth maps: {}'.format(
                len(imgnames_intersect)))

        filenames_depth_intersect = pd.Series(imgnames_intersect).apply(
            lambda x: src_depth_images + (x.replace('.', '_depth.').replace('{}'.format(image_filetype),
                                                                            '{}'.format(depth_image_filetype))))

        X_depth = np.take(X, imgnames_intersect_index, axis=0)
        y_depth = [masks[i] for i in imgnames_intersect_index]
        shapes_depth = np.take(np.asarray(shapes),
                               imgnames_intersect_index, axis=0)
        classes_depth = np.take(np.asarray(
            classes), imgnames_intersect_index, axis=0)

        X_depth_resized = []

        j = 0
        for i in tqdm(imgnames_intersect_index):
            if self.verbose:
                print('\nPreparing image: {}, index: {}'.format(
                    imgnames_intersect[j], j))
            image_depth = imread(filenames_depth_intersect[j])

            if image_depth.shape[0] != X[i].shape[0] or image_depth.shape[1] != X[i].shape[1]:
                if self.verbose:
                    print('Resizing image to: {}'.format(X[i].shape))
                image_depth = resize(
                    image_depth, (X[i].shape[0], X[i].shape[1]), preserve_range=True)

            image_depth = np.expand_dims(image_depth, axis=-1)
            if add_depth_map:
                X_sample = np.dstack([X_depth[j], image_depth])
            else:
                X_sample = X_depth[j]
            X_depth_resized.append(X_sample)
            assert X_sample.shape[0] == y_depth[j].shape[0] and X_sample.shape[1] == y_depth[j].shape[1], "\
            Shapes for original image and depth map must be the same."
            if self.verbose:
                print('Original image shape: {}'.format(X_depth[j].shape))
                if add_depth_map:
                    print('Depth map shape: {}'.format(image_depth.shape))
                print('Final image shape: {}\n'.format(X_sample.shape))
            j += 1

        if self.verbose:
            print('\nFinal number of images containing depth maps: {}'.format(
                len(X_depth_resized)))
        assert len(X_depth_resized) == len(y_depth) == len(
            imgnames_intersect) == len(shapes_depth) == len(classes_depth), "\
        All output files must have the same length."

        return X_depth_resized, y_depth, imgnames_intersect, shapes_depth, classes_depth

    def load_images(self,
                    src_images,
                    image_filetype='JPG',
                    load_masks=False):
        """
        Load raw images from selected directory. Resize them and load masks if prepared.
        """

        ids = np.sort(glob.glob(src_images + '*.{}'.format(image_filetype)))
        if self.verbose:
            print('\nLoad images from: {}\n'.format(src_images))

        X = []
        if load_masks:
            masks = []
        image_shapes = []

        sys.stdout.flush()

        for n, id_ in tqdm(enumerate(ids), total=len(ids)):
            if self.verbose:
                print('\nLoading image: {} with index: {}'.format(id_, n))
            img = imread(id_)
            if len(img) == 2:
                img = img[0]
            if self.verbose:
                print('Image shape: {}'.format(img.shape))
            image_shapes.append(img.shape)
            X.append(img)

            if load_masks:
                if self.verbose:
                    print('Load mask corresponding to an image.')
                mask = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.bool)
                for mask_file in next(os.walk(path + '/masks/'))[2]:
                    mask_ = imread(path + '/masks/' + mask_file)
                    mask_ = resize(mask_, self.image_shape, mode='constant',
                                   preserve_range=True)
                    mask_ = np.expand_dims(mask_, axis=-1)
                    mask = np.maximum(mask, mask_)
                masks.append(mask)

        if self.resize_and_pad:
            if self.verbose:
                print('\nPadding images to: {}'.format(self.image_shape))
            X = self.resize_pad_images(X, shape=self.image_shape)
            if load_masks:
                if self.verbose:
                    print('Padding masks to: {}\n'.format(self.image_shape))
                masks = self.resize_pad_images(masks, shape=self.image_shape)

        if load_masks:
            return X, masks, image_shapes, ids
        else:
            return X, image_shapes, ids

    def resize_pad_images(self,
                          X,
                          masks=None):
        """
        Resize and pad images to selected size.
        """

        if self.verbose:
            print('\nPadding resized images to: {}'.format(self.image_shape))

        X_ = np.zeros(
            (len(X), self.image_shape[0], self.image_shape[1], X[0].shape[-1]))
        masks_ = []

        for i in tqdm(range(len(X))):
            image, window, scale, padding = utils.resize_image(
                X[i],
                min_dim=self.image_shape[0],
                max_dim=self.image_shape[0],
                padding=self.resize_and_pad)
            X_[i] = image
            if masks is not None:
                mask = utils.resize_mask(masks[i], scale, padding)
                masks_.append(mask)

        if masks is not None:
            return X_, masks_
        else:
            return X_

    def create_masks(self,
                     src_images,
                     labels,
                     image_filetype='JPG'):
        """
        Parse selected folder, read images, resize them to size set in
        self.image_shape and create masks based on bounding box coordinates
        in labels DataFrame.
        """

        X = []
        masks = []
        image_names = []
        image_shapes = []
        image_classes = []
        images_list = np.sort(
            glob.glob(src_images + '*.{}'.format(image_filetype)))
        omitted_images = 0

        if self.verbose:
            print('\nCreating masks from: {}'.format(src_images))
            print('Selecting {} images.'.format(image_filetype))
            print('Maximum resized image shape: {}\n'.format(self.image_shape))

        for img in tqdm(images_list):

            image_name = img.split('/')[-1]
            if self.verbose:
                print('\nProcessing image:', img)

            y_image = labels[labels.filename == image_name]

            if y_image.shape[0] == 0:
                if self.verbose:
                    print('Image loading failed.')
                omitted_images += 1
            else:
                image = imread(img)
                if len(image) == 2:
                    if self.verbose:
                        print('Image constituting of two parts, choosing image only.')
                    image = image[0]

                image_shape = image.shape
                mask = np.zeros(
                    (image.shape[0], image.shape[1], y_image.shape[0]), dtype=np.uint8)
                mask_class = []

                if self.verbose:
                    print('Original image shape: {}\nMask shape: {}'.format(
                        image.shape, mask.shape))
                    print('Image min: {}, max: {}'.format(
                        image.min(), image.max()))

                for i in range(y_image.shape[0]):
                    coords = np.array([y_image.iloc[i]['ymin'],
                                       y_image.iloc[i]['ymax'],
                                       y_image.iloc[i]['xmin'],
                                       y_image.iloc[i]['xmax']], dtype=np.int)
                    mask[coords[0]:coords[1], coords[2]:coords[3], i] = 1.
                    mask_class.append(y_image.iloc[i]['class'])

                if image_shape[0] > self.image_shape[0]:
                    resize_factor = self.image_shape[0] / image.shape[0]
                    image_shape_new = (
                        self.image_shape[0], int(resize_factor * image.shape[1]), 3)
                    mask_shape_new = (
                        int(resize_factor * mask.shape[0]), self.image_shape[0], mask.shape[-1])
                if image_shape[1] > self.image_shape[1]:
                    resize_factor = self.image_shape[1] / image.shape[1]
                    image_shape_new = (
                        int(resize_factor * image.shape[0]), self.image_shape[1], 3)
                    mask_shape_new = (
                        int(resize_factor * mask.shape[0]), self.image_shape[1], mask.shape[-1])
                if image_shape[0] > self.image_shape[0] or image_shape[1] > self.image_shape[1]:
                    image = resize(image, image_shape_new,
                                   preserve_range=True).astype(np.uint8)
                    mask = resize(mask, mask_shape_new,
                                  preserve_range=True).astype(np.uint8)

                if self.verbose:
                    print('Mask min: {}, max: {}'.format(
                        mask.min(), mask.max()))
                assert np.min(mask) == 0. and np.max(
                    mask) == 1., "Mask values must be either 0 or 1."
                X.append(image)
                masks.append(mask)
                image_names.append(image_name)
                image_shapes.append(image_shape)
                image_classes.append(mask_class)

                del image, mask
                gc.collect()

        if omitted_images > 0:
            print('\nOmitted images: {}'.format(omitted_images))

        return X, masks, image_names, image_shapes, image_classes

    def perform_image_check(self,
                            src_images,
                            image_names,
                            y,
                            index=None):
        """
        Visualize image and masks to check if they are properly processed.
        """

        if index is None:
            index = np.random.randint(0, len(image_names))
        if not isinstance(image_names, pd.Series):
            image_names = pd.Series(image_names)

        image_check = '{}{}'.format(src_images, image_names[index])
        image_filename = image_check.split('/')[-1]
        img = imread(image_check)

        print('\nChecking image {} from: {}'.format(image_filename, src_images))
        plt.imshow(img)
        self.__show_bbs(y, img, image_filename)

        return

    def __get_center(self,
                     x):
        x['x'] = x.xmax - x.xmin
        x['y'] = x.ymax - x.ymin
        return x

    def __show_bbs(self,
                   y,
                   image,
                   image_name):

        y = y.apply(lambda x: self.__get_center(x), axis=1)
        y_image = y[y.filename == image_name]

        fig, ax = plt.subplots()
        ax.imshow(image)

        for i in range(y_image.shape[0]):
            ax.add_patch(patches.Rectangle((y_image.iloc[i, :]['xmin'],
                                            y_image.iloc[i, :]['ymin']),
                                           y_image.iloc[i, :]['x'],
                                           y_image.iloc[i, :]['y'], facecolor='red'))

        return
