import glob
import os
from argparse import ArgumentParser

from mrcnn_api import kafka_produce_image_filenames

parser = ArgumentParser()
parser.add_argument("--images_source",
                    help="Provide directory containing images to predict.",
                    type=str, default='')
args = parser.parse_args()
print('\nRunning with parameters: {}\n'.format(args))

kafka_produce_image_filenames(args.images_source)
