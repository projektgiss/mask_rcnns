import glob
import os
from argparse import ArgumentParser

import numpy as np
from config import Config
from mrcnn_api import Mrcnn_API

parser = ArgumentParser()
parser.add_argument("--weights_filename",
                    help="Provide trained model weights file.",
                    type=str, default='')
parser.add_argument("--image_filename",
                    help="Provide image filename.",
                    type=str, default='')
args = parser.parse_args()
print('\nRunning with parameters: {}\n'.format(args))


class GISSConfig(Config):

    NAME = "giss_depth"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

    NUM_CLASSES = 1 + 6
    CHOSEN_CLASS = 1
    CHOSEN_CLASS_WEIGHT = 1.

    IMAGE_MIN_DIM = 1024
    IMAGE_MAX_DIM = 1024
    IMAGE_CHANNELS = 3
    IMAGE_PADDING = True

    BACKBONE_STRIDES = [4, 8, 16, 32, 64]
    RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)
    TRAIN_ROIS_PER_IMAGE = 128
    USE_MINI_MASK = True
    MINI_MASK_SHAPE = (56, 56)
    RPN_NMS_THRESHOLD = 0.7
    DETECTION_MAX_INSTANCES = 128
    DETECTION_MIN_CONFIDENCE = 0.7
    DETECTION_NMS_THRESHOLD = 0.3
    LEARNING_RATE = 0.001
    LEARNING_MOMENTUM = 0.9
    WEIGHT_DECAY = 0.0001
    USE_RPN_ROIS = True
    MEAN_PIXEL = np.array([123.7, 116.8, 103.9])
    STEPS_PER_EPOCH = 26
    VALIDATION_STEPS = 7


class InferenceConfig(GISSConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


giss_training_config = {
    'config': GISSConfig(),  # Parameters for MRCNN training configuration.
    # Maximum image size for training, should corresponds to the same parameter in data API.
    'image_shape': (1024, 1024),
    # Whether to resize and pad to specified image_shape.
    'resize_and_pad': True,
    'mrcnn_init': 'coco',  # Training initialization - 'coco' or 'last'.
    # Training weights filename, set to mask_rcnn_coco.h5 for 3 channels, mask_rcnn_coco_4ch.h5 for 4 channels, when using 'coco' init.
    'mrcnn_weights_filename': 'mask_rcnn_coco.h5'
}

api_training = Mrcnn_API(
    config=giss_training_config['config'],
    image_shape=giss_training_config['image_shape'],
    resize_and_pad=giss_training_config['resize_and_pad'],
    verbose=True)

inference_config = InferenceConfig()

class_names = ['BG', 'car', 'building',
               'human', 'truck', 'container', 'winter_car']

print('Load Mask R-CNN model.')
api_training.model_path = args.weights_filename
mrcnn_model_trained = api_training.load_model(inference_config)

image = api_training.load_single_image(
    args.image_filename)

img_preds = api_training.predict_image(image, mrcnn_model_trained, class_names,
                                       visualize_predictions=False)
df_preds = api_training.create_predictions_df(args.image_filename, img_preds)
df_preds.to_json('{}.json'.format(
    args.image_filename.split('/')[-1].split('.')[0]),
    orient='records', lines=False)
json_preds = df_preds.to_json(orient='records', lines=False)[1:-1]

print('Resulting predictions dataframe:\n{}\n'.format(df_preds.head()))
