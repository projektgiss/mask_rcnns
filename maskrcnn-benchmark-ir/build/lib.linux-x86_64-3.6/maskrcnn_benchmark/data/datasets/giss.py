# -*- coding: utf-8 -*-
# File: coco.py

import json
import os

import numpy as np
import pandas as pd
import torch
import torchvision
from maskrcnn_benchmark.structures.bounding_box import BoxList
from maskrcnn_benchmark.structures.segmentation_mask import SegmentationMask
from PIL import Image

GISS_classes = ['cars', 'small_trucks', 'truck',
                'building', 'person', 'container', 'misc']


class GISSDataset(object):

    def __init__(self, ann_file,
                 root,
                 transforms=None):

        self.ann_file = ann_file
        self.root = root
        self.transforms = transforms

        self.df_annots = pd.read_csv(self.ann_file)
        self.df_annots = self.__clean_slashes_df(self.df_annots)
        self.ids = self.df_annots.filename.unique().tolist()

    def __len__(self):
        return len(self.ids)

    def __getitem__(self, idx):

        df_filename = self.df_annots[self.df_annots['filename'] == self.ids[idx]]

        boxes = df_filename.apply(
            lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        boxes = np.stack(
            boxes.values, axis=0)
        filename_full = self.root + self.ids[idx]

        img = Image.open(filename_full)

        boxes = torch.as_tensor(boxes).reshape(-1, 4)
        target = BoxList(boxes, img.size, mode="xyxy")

        classes = df_filename['class'].values
        classes = torch.tensor(classes)
        target.add_field("labels", classes)

        # masks = [obj["segmentation"] for obj in anno]
        # masks = SegmentationMask(masks, img.size)
        # target.add_field("masks", masks)

        target = target.clip_to_image(remove_empty=True)

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target, idx

    def get_img_info(self, index):

        filename_full = self.root + self.ids[index]
        img = Image.open(filename_full)

        return {"height": img.size[1], "width": img.size[0]}

    def __clean_slashes_df(self, df_annots):

        df_annots.filename = df_annots.filename.str.replace("\\", "/")

        return df_annots

    def load_annotation_df(self):

        self.df_annots = pd.read_csv(self.ann_file)
        self.df_annots = self.__clean_slashes_df(self.df_annots)
        self.ids = self.df_annots.filename.unique().tolist()

        return self.df_annots
