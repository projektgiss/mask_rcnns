import colorsys
import gc
import glob
import math
import os
import random
import re
import sys
import time

import matplotlib
import matplotlib.pyplot as plt
import model as modellib
import numpy as np
import pandas as pd
import utils
import visualize
from config import Config
from matplotlib import patches
from model import log
from skimage.io import concatenate_images, imread, imread_collection, imshow
from skimage.transform import resize
from sklearn.utils import shuffle
from tqdm import tqdm


class Mrcnn_API(object):

    def __init__(self,
                 config,
                 image_shape=(1024, 1024),
                 resize_and_pad=True,
                 model_path=None,
                 model_dir=None,
                 verbose=False):

        self.config = config
        self.image_shape = image_shape
        self.resize_and_pad = resize_and_pad
        self.model_path = model_path
        self.model_dir = model_dir if model_dir is not None else os.path.join(
            os.getcwd(), "logs")
        self.verbose = verbose

    def train_model(self,
                    dataset_train,
                    dataset_val,
                    init_with="last",
                    weights_file=None,
                    train_heads=True,
                    epochs_heads=10,
                    epochs_full=20,
                    class_weight=None):

        assert init_with == 'last' or init_with == 'coco', "\
        Weights must be initialized either with 'last' or 'coco'."

        if self.verbose:
            print('Creating MRCNN model instance.')
        model = modellib.MaskRCNN(mode="training", config=self.config,
                                  model_dir=self.model_dir)

        if init_with == "coco":
            model.load_weights(weights_file, by_name=True,
                               exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",
                                        "mrcnn_bbox", "mrcnn_mask"])
            if self.verbose:
                print('COCO weights loaded.')
        elif init_with == "last":
            model.load_weights(weights_file, by_name=True)
            if self.verbose:
                print('Last weights loaded.')

        if train_heads:
            model.train(dataset_train, dataset_val,
                        learning_rate=self.config.LEARNING_RATE,
                        epochs=epochs_heads,
                        layers='heads',
                        class_weight=class_weight)

        model.train(dataset_train, dataset_val,
                    learning_rate=self.config.LEARNING_RATE,
                    epochs=epochs_full,
                    layers="all",
                    class_weight=class_weight)

        return model

    def predict_image(self,
                      image,
                      model,
                      class_names,
                      visualize_predictions=False):

        t_start = time.time()
        results = model.detect([image], verbose=1)
        t_end = time.time()
        if self.verbose:
            print('Time it took for inference: {}'.format(t_end - t_start))

        r = results[0]

        if visualize_predictions:
            visualize.display_instances(image,
                                        r['rois'], r['masks'],
                                        r['class_ids'],
                                        class_names, r['scores'],
                                        ax=self.__get_ax(size=18),
                                        figsize=(14, 14))
        return r

    def create_predictions_df(self,
                              filename,
                              r):

        if self.verbose:
            print('Image filename: {}'.format(filename))

        df_bb = pd.DataFrame(r['rois'])
        df_bb.columns = ['ymin', 'xmin', 'ymax', 'xmax']
        df_bb['filename'] = filename
        df_bb['class'] = r['class_ids']
        df_bb['confidence'] = r['scores']
        df_bb = df_bb.apply(lambda x: self.__get_center(x), axis=1)

        return df_bb

    def load_model(self,
                   inference_config):

        model = modellib.MaskRCNN(mode="inference",
                                  config=inference_config,
                                  model_dir=self.model_dir)

        assert self.model_path != "", "Provide path to trained weights"
        if self.verbose:
            print("Loading weights from: {}".format(self.model_path))
        model.load_weights(self.model_path, by_name=True)

        return model

    def load_single_image(self,
                          src):

        if self.verbose:
            print('Loading image: {}'.format(src))

        image = imread(src)
        if len(image) == 2:
            image = image[0]
        if self.verbose:
            print('Original image shape: {}'.format(image.shape))

        if self.resize_and_pad:
            image = self.__resize_pad_image(image)
            if self.verbose:
                print('Image padded to: {}'.format(image.shape))

        return image

    def __resize_pad_image(self,
                           image_,
                           mask_=None):

        image, window, scale, padding = utils.resize_image(
            image_,
            min_dim=self.config.IMAGE_MIN_DIM,
            max_dim=self.config.IMAGE_MAX_DIM,
            padding=self.config.IMAGE_PADDING)
        if mask_ is not None:
            mask = utils.resize_mask(mask_, scale, padding)
            return image, mask

        return image

    def __get_center(self,
                     x):
        x['x'] = x.xmax - x.xmin
        x['y'] = x.ymax - x.ymin
        return x

    def __get_ax(self,
                 rows=1,
                 cols=1,
                 size=8):
        _, ax = plt.subplots(rows, cols, figsize=(size * cols, size * rows))
        return ax
