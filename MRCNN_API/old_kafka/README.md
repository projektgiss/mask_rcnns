# Mask R-CNN Prediction API for GISS data

### Structure:

Class & additional functions are located in `mrcnn_api.py`.

### Parameters:

To predict on an image run `mrcnn_api_predict.py` providing model weights as first argument and image filename as second argument.

#### Example usage:

```python
python mrcnn_api_predict.py --weights_file mask_rcnn_giss_0073.h5 --image_filename giss_images/DJI_0010.JPG
```

Model configuration is stored in `mrcnn_api.py` in `GISSConfig`.

## Kafka usage (WIP):

1.  In kafka directory run `bin/zookeeperver-start.sh config/zookeeper.properties` in one terminal window.
2.  In kafka dir run `bin/kafka-server-start.sh config/server.properties`in 2nd terminal window.
3.  Feed Kafka producer names of images to predict: `python kafka_produce_mrcnn_filenames.py --images_source giss_images/`, where `giss_images/` is directory containing images for which predictions should be made.
4.  Consume created messages with `python kafka_mrcnn_api_predict.py --weights_file mask_rcnn_giss_0073.h5`, providing weights of the model to predict with.
5.  Predictions are saved into serialized AVRO file with `save_to_avro` function, by default `mrcnn.avro`, which can be read with `read_avro('mrcnn.avro')`.
