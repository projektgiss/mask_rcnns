# -*- coding: utf-8 -*-
# File: coco.py

import json
import os

import config
import cv2
import numpy as np
import pandas as pd
from pycocotools.coco import COCO
from tabulate import tabulate
from tensorpack.utils import logger
from tensorpack.utils.argtools import log_once
from tensorpack.utils.rect import FloatBox
from tensorpack.utils.timer import timed_operation
from termcolor import colored

__all__ = ['GISSDetection']

GISS_NUM_CATEGORY = 7
GISS_classes = ['cars', 'small_trucks', 'truck',
                'building', 'person', 'container', 'misc']

config.NUM_CLASS = GISS_NUM_CATEGORY + 1
config.CLASS_NAMES = ['BG'] + GISS_classes


class GISSDetection(object):

    def __init__(self,
                 basedir=config.BASEDIR,
                 depth_basedir=config.DEPTH_BASEDIR,
                 name=config.TRAIN_DATASET,
                 ):

        self.basedir = basedir
        self.depth_basedir = depth_basedir
        self.name = name

    def load_annotations(self, df_src):

        all_dicts = []

        df_annots = pd.read_csv(df_src)
        df_annots = self.__clean_slashes_df(df_annots)

        df_annots['set'] = df_annots.filename.apply(lambda x: x.split('/')[0])
        df_annots['image_filename'] = df_annots.filename.apply(lambda x: x.split('/')[1])
        df_annots['depth_set'] = df_annots.set.apply(lambda x: '{}_depth'.format(x))
        df_annots['depth_filename'] = df_annots.image_filename.apply(lambda x: x.split('.')[0] + '_depth.png')
        df_annots['full_depth_filename'] = df_annots.apply(lambda x: '{}{}/{}'.format(
            config.DEPTH_BASEDIR, x.depth_set, x.depth_filename), axis=1)

        unique_filenames = df_annots.filename.unique()

        for i in range(len(unique_filenames)):

            df_filename = df_annots[df_annots['filename'] == unique_filenames[i]]

            filename_boxes = df_filename.apply(
                lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
            filename_boxes = np.stack(
                filename_boxes.values, axis=0).astype(np.float32)
            filename_full = self.basedir + unique_filenames[i]

            assert len(df_filename.full_depth_filename.unique()) == 1
            depth_filename_full = df_filename.full_depth_filename.iloc[0]

            # img = cv2.imread(filename_full)
            img_depth = cv2.imread(depth_filename_full, 0)
            if img_depth is not None:
                # h, w = img.shape[0], img.shape[1]

                filename_dict = {}
                # filename_dict['height'] = h
                # filename_dict['width'] = w
                filename_dict['id'] = filename_full
                filename_dict['file_name'] = [filename_full, depth_filename_full]
                filename_dict['boxes'] = filename_boxes
                filename_dict['class'] = df_filename['class'].values
                filename_dict['is_crowd'] = np.zeros((filename_boxes.shape[0]))

                all_dicts.append(filename_dict)

        return all_dicts

    def output_json_annotations(self, df_src, output_file):

        all_dicts = self.load_annotations(df_src)
        with open(output_file, 'w') as f:
            json.dump(all_dicts, f)

        return

    def __clean_slashes_df(self, df_annots):

        df_annots.filename = df_annots.filename.str.replace("\\", "/")

        return df_annots


class mAPevaluate(object):

    def __init__(self,
                 df_true_src,
                 df_preds_src,
                 data_src,
                 iou_threshold=0.5):

        self.df_true_src = df_true_src
        self.df_preds_src = df_preds_src
        self.data_src = data_src
        self.iou_threshold = iou_threshold
        self.class_dict = None

    def process_df_true(self):

        df_true = pd.read_csv(self.df_true_src)

        df_true.filename = df_true.filename.str.replace("\\", "/")
        df_true['full_filename'] = df_true.filename.apply(
            lambda x: '{}{}'.format(self.data_src, x))

        list_classes = list(range(1, 8))
        self.class_dict = dict(zip(GISS_classes, list_classes))

        return df_true

    def process_df_preds(self):

        df_preds = pd.read_json(self.df_preds_src)

        df_preds['xmin'] = df_preds.bbox_raw.apply(lambda x: x[0])
        df_preds['xmax'] = df_preds.bbox_raw.apply(lambda x: x[2])
        df_preds['ymin'] = df_preds.bbox_raw.apply(lambda x: x[1])
        df_preds['ymax'] = df_preds.bbox_raw.apply(lambda x: x[3])
        df_preds.category_id = df_preds.category_id.map(self.class_dict)

        return df_preds

    def output_processed_dfs(self):

        df_true = self.process_df_true()
        df_preds = self.process_df_preds()

        return df_true, df_preds

    def prepare_mAP_input(self, img_true, img_preds, img_id):

        gt_boxes = img_true.apply(
            lambda x: np.asarray([x['xmin'], x['ymin'], x['xmax'], x['ymax']]), axis=1)
        gt_boxes = np.stack(gt_boxes.values, axis=0).astype(np.float32)

        gt_class_ids = img_true['class'].values

        pred_boxes = img_preds.bbox_raw
        pred_boxes = np.stack(pred_boxes.values, axis=0).astype(np.float32)

        pred_class_ids = img_preds.category_id.values
        pred_scores = img_preds.score.values

        return gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores

    def compute_batch_ap(self):

        df_true = self.process_df_true()
        df_preds = self.process_df_preds()
        unique_filenames = df_true.full_filename.unique()

        imgs_no_preds = 0
        orig_missed_images = {}
        APs = []

        for i in unique_filenames:
            img_true = df_true[df_true.full_filename == i]
            img_preds = df_preds[df_preds.image_id == i]
            if img_preds.shape[0] == 0 and img_true.shape[0] != 0:
                AP = 0
                imgs_no_preds += 1
                orig_missed_images[i] = img_true.shape[0]
            else:
                gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores = self.prepare_mAP_input(
                    img_true, img_preds, i)
                AP, precisions, recalls, overlaps = compute_ap(
                    gt_boxes, gt_class_ids, pred_boxes, pred_class_ids, pred_scores,
                    iou_threshold=self.iou_threshold)
            APs.append(AP)

        mAP = np.mean(APs)
        print('mAP at IoU threshold {}: {}'.format(self.iou_threshold, mAP))
        print('Images, where no objects were found: {}'.format(imgs_no_preds))

        return mAP, orig_missed_images


# Based on Matterports MRCNN functions
def compute_iou(box, boxes, box_area, boxes_area):
    """Calculates IoU of the given box with the array of the given boxes.
    box: 1D vector [y1, x1, y2, x2]
    boxes: [boxes_count, (y1, x1, y2, x2)]
    box_area: float. the area of 'box'
    boxes_area: array of length boxes_count.

    Note: the areas are passed in rather than calculated here for
          efficency. Calculate once in the caller to avoid duplicate work.
    """
    # Calculate intersection areas
    y1 = np.maximum(box[0], boxes[:, 0])
    y2 = np.minimum(box[2], boxes[:, 2])
    x1 = np.maximum(box[1], boxes[:, 1])
    x2 = np.minimum(box[3], boxes[:, 3])
    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)
    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union
    return iou


def compute_overlaps(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].

    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes
    area1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1])
    area2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1])

    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))
    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]
        overlaps[:, i] = compute_iou(box2, boxes1, area2[i], area1)
    return overlaps


def trim_zeros(x):
    """It's common to have tensors larger than the available data and
    pad with zeros. This function removes rows that are all zeros.

    x: [rows, columns].
    """
    assert len(x.shape) == 2
    return x[~np.all(x == 0, axis=1)]


def compute_ap(gt_boxes, gt_class_ids,
               pred_boxes, pred_class_ids, pred_scores,
               iou_threshold=0.5):
    """Compute Average Precision at a set IoU threshold (default 0.5).

    Returns:
    mAP: Mean Average Precision
    precisions: List of precisions at different class score thresholds.
    recalls: List of recall values at different class score thresholds.
    overlaps: [pred_boxes, gt_boxes] IoU overlaps.
    """
    # Trim zero padding and sort predictions by score from high to low
    # TODO: cleaner to do zero unpadding upstream
    gt_boxes = trim_zeros(gt_boxes)
    pred_boxes = trim_zeros(pred_boxes)
    pred_scores = pred_scores[:pred_boxes.shape[0]]
    indices = np.argsort(pred_scores)[::-1]
    pred_boxes = pred_boxes[indices]
    pred_class_ids = pred_class_ids[indices]
    pred_scores = pred_scores[indices]

    # Compute IoU overlaps [pred_boxes, gt_boxes]
    overlaps = compute_overlaps(pred_boxes, gt_boxes)

    # Loop through ground truth boxes and find matching predictions
    match_count = 0
    pred_match = np.zeros([pred_boxes.shape[0]])
    gt_match = np.zeros([gt_boxes.shape[0]])
    for i in range(len(pred_boxes)):
        # Find best matching ground truth box
        sorted_ixs = np.argsort(overlaps[i])[::-1]
        for j in sorted_ixs:
            # If ground truth box is already matched, go to next one
            if gt_match[j] == 1:
                continue
            # If we reach IoU smaller than the threshold, end the loop
            iou = overlaps[i, j]
            if iou < iou_threshold:
                break
            # Do we have a match?
            if pred_class_ids[i] == gt_class_ids[j]:
                match_count += 1
                gt_match[j] = 1
                pred_match[i] = 1
                break

    # Compute precision and recall at each prediction box step
    precisions = np.cumsum(pred_match) / (np.arange(len(pred_match)) + 1)
    recalls = np.cumsum(pred_match).astype(np.float32) / len(gt_match)

    # Pad with start and end values to simplify the math
    precisions = np.concatenate([[0], precisions, [0]])
    recalls = np.concatenate([[0], recalls, [1]])

    # Ensure precision values decrease but don't increase. This way, the
    # precision value at each recall threshold is the maximum it can be
    # for all following recall thresholds, as specified by the VOC paper.
    for i in range(len(precisions) - 2, -1, -1):
        precisions[i] = np.maximum(precisions[i], precisions[i + 1])

    # Compute mean AP over recall range
    indices = np.where(recalls[:-1] != recalls[1:])[0] + 1
    mAP = np.sum((recalls[indices] - recalls[indices - 1]) *
                 precisions[indices])

    return mAP, precisions, recalls, overlaps
