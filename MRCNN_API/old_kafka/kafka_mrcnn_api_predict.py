import glob
import os
from argparse import ArgumentParser

from mrcnn_api import (MRCNN_API, GISSConfig, kafka_consume_and_predict,
                       kafka_produce_image_filenames)

parser = ArgumentParser()
parser.add_argument("--weights_file",
                    help="Provide trained model weights file.",
                    type=str, default='')
args = parser.parse_args()
print('\nRunning with parameters: {}\n'.format(args))

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"


config = GISSConfig()
class_names = ['BG', 'car', 'building',
               'human', 'truck', 'container', 'winter_car']

mrcnn_model = MRCNN_API(config, model_path=args.weights_file)
kafka_consume_and_predict(mrcnn_model, class_names)
