# Mask R-CNN Prediction API for GISS data

### Structure:

Class & additional functions are located in `mrcnn_api.py`.

### Parameters:

To predict on an image run `mrcnn_api_predict.py` providing model weights as first argument and image filename as second argument.

#### Example usage:

```python
python mrcnn_api_predict.py --weights_file mask_rcnn_giss_0073.h5 --image_filename giss_images/DJI_0010.JPG
```

Model configuration is stored in `mrcnn_api.py` in `GISSConfig`.
