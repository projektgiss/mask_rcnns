logdir='train_log/resnet50_w1_4ch'
load='pretrained_models/ImageNet-ResNet50_4ch_random.npz'

evaluation_set='val'  # val or test
gpu=1


python train.py \
  --gpu $gpu \
  --logdir $logdir \
  --load $load \
  --evaluation_set $evaluation_set
